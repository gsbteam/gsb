# README #

Équipe de projet : Gaël FONTAINE & Anaïs DELATTRE

### Mise en place de la base de données ###

* Base de données : gsb_frais
* Exécuter gsb_frais_structure.sql
* Exécuter gsb_frais_insert_tables_statiques.sql

### Documentation ###

Documentation générée par Doxygen
Accès à la documentation dans le dossier "documentation" du projet.