<?php

require("../include/_init.inc.php");
header('Content-Type: application/json');

$utilisateur = $_POST['idUser'];
$idHorsForfait = $_POST['id'];
$mois = $_POST['mois'];

// On regarde quel mois vient après celui passé en paramètre
$moisReport = obtenirMoisSuivant($mois);
// On on reporte le frais hors forfait dans la fiche de frais du mois suivant
$idJeuEltsMois = $pdo->reporterHorsForfait($idHorsForfait, $moisReport);
// On vérifie que la fiche de frais du mois suivant existe pour cet utilisateur et pour le mois suivant
$existeFicheFrais = $pdo->getLesInfosFicheFrais($utilisateur, $moisReport);
// Si elle n'existe pas, on la crée
if (!$existeFicheFrais) {
    $pdo->creeNouvellesLignesFrais($utilisateur, $moisReport);
}
