// Listes déroulantes liées
$(document).ready(function () {
    var lesVisit = $('#lstVisit');
    var lesMois = $('#lstMois');
    // A la sélection d'un visiteur dans la liste lstVisit
    lesVisit.on('change', function () {
        var val = $(this).val(); //On récupère les mois où le visiteur selectionné a une fiche de frais
        if (val !== '') {
            lesMois.empty();
            //On charge la liste des mois
            $.ajax({
                url: '../vues/V_recupInfoMois.php',
                type: 'post',
                data: 'idUtilisateur=' + val,
                success: function (json) {
                    $.each(json, function (index) {
                        lesMois.append('<option value="' + json[index]['mois'] + '">' + json[index]['libelle'] + " " +
                                json[index]['numAnnee'] + '</option>');
                    });
                }
            });
        }
    });

    // Action sur le bouton Refuser --> refuse un frais hors forfait
    $('.btnRefuse').click(function () {
        parametre = $(this).parents("tr").attr('id');
        paramSplit = parametre.split('_');
        id = paramSplit[0];
        $.ajax({
            url: '../vues/V_refusForfait.php',
            type: 'POST',
            data: 'id=' + id,
            success: function () {
                window.location.reload(false);
            }
        });
    });

    // Validation des modifications --> modifie dynamiquement les valeurs dans la base de données
    $('.qteForfait').change(function () {
        id = $(this).attr('id');
        quantite = $(this).val();
        mois = $('.idMois').attr('id');
        idVisiteur = $('.idUtilisateur').attr('id');
        $.ajax({
            url: '../vues/V_valModif.php',
            type: 'POST',
            data: 'idFrais=' + id + '&quantite=' + quantite + '&idVisiteur=' + idVisiteur + '&mois=' + mois,
            success: function () {
            }
        });
    });

// Action sur le bouton Valider
// Calcul le montant validé et passe l'état de la fiche à "Validée"
    $('.btnValider').click(function () {
        idFrais = $(this).attr('id');
        quantite = $(this).val();
        idMois = $('.idMois').attr('id');
        idUtilisateur = $('.idUtilisateur').attr('id');
        $.ajax({
            url: '../vues/V_ficheValid.php',
            type: 'POST',
            data: 'idUtilisateur=' + idUtilisateur + '&mois=' + idMois + '&idFrais=' + idFrais + '&quantite' + quantite,
            success: function () {
                window.location.reload(true);
            }
        });
    });

    // Action sur le bouton Reporter --> reporte un frais hors forfait au mois suivant
    $('.btnReport').click(function () {
        parametre = $(this).parents("tr").attr('id');
        mois = $(this).parents("td").attr('mois');
        idUtilisateur = $('.idUtilisateur').attr('idUser');
        paramSplit = parametre.split('_');
        id = paramSplit[0];
        mois = paramSplit[1];
        $.ajax({
            url: '../vues/V_reportForfait.php',
            type: 'POST',
            data: 'idUser=' + idUtilisateur + '&id=' + id + '&mois=' + mois,
            success: function () {
                window.location.reload(false);
            }
        });
    });
});